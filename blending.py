import datetime
import json
import math
import os
from time import sleep

import moderngl
import moderngl_window as mglw
import numpy as np
from moderngl_window.geometry import quad_2d
from pyrr import Matrix44
from pyrr import Vector3
from shapely import geometry

from lib.app import XvfbApp
from lib.xvfbcapture import XVFBCaptureAsync
from lib.grids import Grid
from lib.output import OutputManager
from lib.remote import RemoteControlThread
from lib.xvfbcapture import XVFBCaptureAsync
from lib.shmread import SHMRead

# hue hue kurwa małpie łatanie (ang monkey patching)
def get_local_window_cls(*args):
    return mglw.get_window_cls('lib.context.pyglet.Window')


mglw.get_local_window_cls = get_local_window_cls

XD = (400, 300)
VGA = (640, 480)
HD_1080 = (1920, 1008)
WUXGA = (1920, 1200)
DUPA = (500, 500)

RESOLUTIONS = {
    'DUPA': DUPA,
    'WUXGA': WUXGA,
    'HD_1080': HD_1080
}

CONFIG_PATH = 'config.json'

class Settings:
    def __init__(self, app):
        self.app = app
        f = open(CONFIG_PATH, 'r')
        self.data = json.load(f)
        f.close()
        self.update_data()
    
    def update_data(self):
        outputs = self.data['outputs']
        for out in outputs:
            res = out[0]
            if res is str and res in RESOLUTIONS:
                out[0] = RESOLUTIONS[res]

        self.data['outputs'] = outputs

    def save(self):
        c = 0
        for out in self.app.output_manager.monitors:
            self.data['outputs'][c][1] = out.position
            c += 1
        with open(CONFIG_PATH, 'w') as cfg:
            json.dump(self.data, cfg, indent=4)

    @classmethod
    def get_window_size(cls):
        f = open(CONFIG_PATH, 'r')
        data = json.load(f)
        f.close()

        if 'window_size' in data:
            return tuple(data['window_size'])
        else:
            return (1200, 600)


class App(mglw.WindowConfig):
    gl_version = (3, 3)
    title = "Blendzior"
    window_size = Settings.get_window_size()
    resizable = True
    samples = 1

    resource_dir = 'data'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @classmethod
    def run(cls):
        mglw.run_window_config(cls)


class Blending(App):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.app_settings = Settings(self)
        self.output_manager = OutputManager(self.app_settings.data['outputs'], app=self, ctx=self.ctx)
        self.negative = False
        self.debug_mask = False
        self.mask_curve_updated = True
        self.fill_curve_updated = True
        self.xvfb_reader = None

        self.render_lock = False

        total = self.output_manager.total
        print("Screen size total:", total)
        #if 'app' in self.app_settings.data and self.app_settings.data['app']:
        #    app = XvfbApp(total, self.app_settings)
        #    app.daemon = True
        #    app.start()
        
        #    cnt = 0
        #    while True:
        #        cnt += 1
        #        if cnt > 20:
        #            raise Exception('Unable to find  /tmp/Xvfb_screen0')
        #        if os.path.exists('/tmp/Xvfb_screen0'):
        #            self.xvfb_reader = True
        #            break
        #        sleep(0.5)
    
        grid = Grid(width=total[0], height=total[1])

        surf_data = grid.draw()

        self.grid_texture = self.ctx.texture(total, 4, surf_data)
        self.live_texture = self.ctx.buffer(reserve=total[0]*total[1]*4)
        self.projection_plane = quad_2d(total, (total[0]/2, total[1]/2))
        self.output_plane = quad_2d(total, (total[0]/2, total[1]/2))

        if 'app' in self.app_settings.data:
            self.xvfb_reader = True
            self.shmreader = SHMRead(98352, total)
            self.reader_async = XVFBCaptureAsync(self.shmreader, self)
            self.reader_async.daemon = True
            #print("START")
            #self.reader_async.start()

        # projection plane shader        
        self.projection_prog = self.load_program('projection_plane.glsl')
        self.mvp = self.projection_prog['Mvp']
        self.swap_channels = self.projection_prog['swap']
        self.image_negative = self.projection_prog['negative']
        self.darken = self.projection_prog['darken']
        self.proj_debug = self.projection_prog['debug']
        self.proj_debug.value = False

        # edging xD masks shader
        self.mask_prog = self.load_program('masks.glsl')
        self.mvp_mask = self.mask_prog['Mvp']
        self.mask_direction = self.mask_prog['direction']
        self.mask_debug = self.mask_prog['debug']

        self.masks_curve = self.mask_prog['c1'], self.mask_prog['c2'], self.mask_prog['c3'], self.mask_prog['c4']   

        self.update_mask_curve()

        # output plane used for final draw and add BW levels
        self.output_prog = self.load_program('output_plane.glsl')
        self.output_mvp = self.output_prog['output_mvp']
        self.output_mvp.write(Matrix44.orthogonal_projection(0, self.window_size[0], 0, self.window_size[1], -100, 1000).astype('f4').tobytes())
        self.ctx.enable(moderngl.BLEND)
        self.fill_curve = self.output_prog['c1'], self.output_prog['c2'], self.output_prog['c3'], self.output_prog['c4']   

        self.out_texture = self.ctx.texture(total, components=4)
        self.unmasked_texture = self.ctx.texture(total, components=4)
        self.output_manager.draw_unmask_bitmap()
        self.virt_fbo = self.ctx.framebuffer(self.out_texture)

        self.output_prog['Texture'].value = 1
        self.output_prog['Unmask'].value = 2
        if self.xvfb_reader:
            self.swap_channels.value = True
        remote = RemoteControlThread(self)
        remote.setDaemon(True)
        remote.start()
        self.grab_cnt = 0
        #self.xfile = open('/tmp/Xvfb_screen0', 'rb')


    def update_mask_curve(self):
        if not self.mask_curve_updated:
            return
        c = 0
        vals = self.app_settings.data['edgeCurve']
        for uniform in self.masks_curve:
            uniform.value = vals[c]
            c+=1
        self.mask_curve_updated = False

    def update_fill_curve(self):
        if not self.fill_curve_updated:
            return
        c = 0
        vals = self.app_settings.data['eqLevelsCurve']
        for uniform in self.fill_curve:
            uniform.value = vals[c]
            c+=1
        self.fill_curve_updated = False

    def key_event(self, key, action, modifiers):
        if action == self.wnd.keys.ACTION_PRESS:
            if key == self.wnd.keys.N:
                self.toggle_negative()

            if key == self.wnd.keys.M:
                self.toggle_mask_debug()

            if key == self.wnd.keys.P:
                self.output_manager.toggle_precission()

            if key == self.wnd.keys.C:
                self.output_manager.toggle_camera()

        camera = self.output_manager.get_active_camera()
        if not camera:
            return
        if action == self.wnd.keys.ACTION_PRESS:
            if key == self.wnd.keys.W:
                camera.move_up()
            if key == self.wnd.keys.S:
                camera.move_down()
            if key == self.wnd.keys.A:
                camera.move_left()
            if key == self.wnd.keys.D:
                camera.move_right()

    def toggle_negative(self):
        self.negative = not self.negative

    def toggle_mask_debug(self):
        self.debug_mask = not self.debug_mask

    def render_main(self):
        self.image_negative.value = self.negative
        self.grid_texture.use()
        self.projection_plane.render(program=self.projection_prog)

    def render(self, time: float, frame_time: float):
        #print("RENDER", self.grab_cnt)

        if self.xvfb_reader and self.grab_cnt == 0:
            d_start = datetime.datetime.now()
            self.grid_texture.write(memoryview(self.shmreader.read()))
            d_end = datetime.datetime.now()
            print((d_end - d_start).total_seconds())
        
        self.grab_cnt += 1
        if self.grab_cnt > 2:
            self.grab_cnt = 0
        

        if self.output_manager.unmask_data:
            self.unmasked_texture.write(self.output_manager.unmask_data)
            self.output_manager.unmask_data = None

        if self.render_lock:
            print("RENDER PAUSED")
            return
        
        self.virt_fbo.use()
        self.virt_fbo.clear(.0, .0, .0)
        self.mask_debug.value = self.debug_mask
        for out in self.output_manager.monitors:
            out.render()

        self.update_mask_curve()
        self.update_fill_curve()

        self.wnd.fbo.use()
        self.ctx.viewport = (0, 0, self.window_size[0], self.window_size[1])
        self.out_texture.use(1)
        self.unmasked_texture.use(2)
        
        self.output_plane.render(program=self.output_prog)

if __name__ == '__main__':
    Blending.run()
