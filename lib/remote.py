import threading
import uuid

import paho.mqtt.client as mqtt

SERVER_HOST = '127.0.0.1'


class RemoteControlThread(threading.Thread):
    def __init__(self, app):
        threading.Thread.__init__(self)
        self.app = app

    def on_move_message(self, client, userdata, message):
        self.app.render_lock = True
        direction, jump = message.payload.decode('utf-8').split(",")
        jump = int(jump)

        camera = self.app.output_manager.get_active_camera()
        if not camera:
            return

        if direction == "up":
            camera.move_up(jump)
        if direction == "down":
            camera.move_down(jump)
        if direction == "left":
            camera.move_left(jump)
        if direction == "right":
            camera.move_right(jump)

        self.app.render_lock = False

    def on_toggle_cam_message(self, client, userdata, message):
        self.app.output_manager.toggle_camera()

    def on_update_mask_curve_message(self, client, userdata, message):
        data = message.payload.decode('utf-8').split(':')
        c = 0
        for val in data:
            self.app.app_settings.data['edgeCurve'][c] = float(val)
            c += 1
        self.app.mask_curve_updated = True
        self.app.app_settings.save()
        print("Mask updated")

    def on_update_fill_curve_message(self, client, userdata, message):
        data = message.payload.decode('utf-8').split(':')
        c = 0
        for val in data:
            self.app.app_settings.data['eqLevelsCurve'][c] = float(val)
            c += 1
        self.app.fill_curve_updated = True
        self.app.app_settings.save()
        print("Mask updated")

    def on_connect(self, client, userdata, flags, rc):
        print("Connected with result code "+str(rc))

        channel = "blendzior/move"
        client.subscribe(channel)
        client.message_callback_add(channel, self.on_move_message)
        print("Subscribing to %s" % channel)

        channel = "blendzior/toggle_cam"
        client.subscribe(channel)
        client.message_callback_add(channel, self.on_toggle_cam_message)
        print("Subscribing to %s" % channel)

        channel = "blendzior/mask_curve"
        client.subscribe(channel)
        client.message_callback_add(channel, self.on_update_mask_curve_message)
        print("Subscribing to %s" % channel)

        channel = "blendzior/fill_curve"
        client.subscribe(channel)
        client.message_callback_add(channel, self.on_update_fill_curve_message)
        print("Subscribing to %s" % channel)

    def on_disconnect(self, *args, **kwargs):
        print("DISCO", args, kwargs)

    def run(self):
        client_id = 'blendzior_app_' + str(uuid.uuid4())

        self.client = mqtt.Client(client_id=client_id)
        self.client.on_connect = self.on_connect

        self.client.connect(SERVER_HOST, 1883, 60)
        self.client.on_disconnect = self.on_disconnect

        print("Start loop...")
        self.client.loop_forever()
