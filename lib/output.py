import numpy as np
from moderngl_window.geometry import quad_2d
from PIL import Image
from PIL import ImageDraw
from pyrr import Matrix44
from pyrr import Vector3
from pyrr import matrix44
from shapely import geometry

MASK_TOP = 1
MASK_BOTTOM = 2
MASK_LEFT = 3
MASK_RIGHT = 4


def bounds_to_points(bounds):
    min_x, min_y, max_x, max_y = bounds
    p_a = (min_x, min_y)
    p_b = (max_x, min_y)
    p_c = (max_x, max_y)
    p_d = (min_x, max_y)

    return p_a, p_b, p_c, p_d


class Mask:
    def __init__(self, monitor, ctx, app):
        self.coords = (0, 0, 0, 0)
        self.ctx = ctx
        self.app = app
        self.quad = quad_2d((1, 1))

        self.translate = None
        self.scale = None

        self.set_matrices()

        self.projection = Matrix44.orthogonal_projection(
            0, monitor.size[0], monitor.size[1], 0, -100, 1000)

    def set_matrices(self):
        # translate * rotate * scale
        pos = self.coords
        dims = self.dimensions
        trans = [pos[0]+dims[0]/2, pos[1] + dims[1]/2, 1]
        self.translate = matrix44.create_from_translation(np.array(trans))
        self.scale = Matrix44.from_scale(np.array([dims[0], dims[1], 1]))

    @property
    def direction(self):
        pos = self.coords
        width, height = self.dimensions
        if width > height:
            if pos[1] == 0.0:
                return MASK_TOP
            else:
                return MASK_BOTTOM
        else:
            if pos[0] == 0.0:
                return MASK_LEFT
            else:
                return MASK_RIGHT

    @property
    def is_active(self):
        min_x, min_y, max_x, max_y = self.coords
        return max_x > min_x and max_y > min_y

    def set_coords(self, coords):
        self.coords = coords
        self.set_matrices()

    def get_projection_matrix(self):
        return self.projection * self.translate * self.scale

    def render(self):
        if not self.is_active:
            return
        self.app.mask_direction.value = self.direction
        self.app.mvp_mask.write(
            self.get_projection_matrix().astype('f4').tobytes())
        self.quad.render(program=self.app.mask_prog)

    @property
    def dimensions(self):
        min_x, min_y, max_x, max_y = self.coords
        p_a = (min_x, min_y)
        p_c = (max_x, max_y)
        p_d = (min_x, max_y)

        width = p_c[0] - p_d[0]
        height = p_d[1] - p_a[1]
        return width, height


class Output:
    def __init__(self, size, position, physical_position, manager, app):
        self.size = size
        self.position = position
        self.physical_position = physical_position
        self.ctx = manager.ctx
        self.app = app
        self.manager = manager

        self.polygon = None
        self.init_polygon()

        self.masks = []
        self.unmasked = None
        self.init_masks()

        self.idx = None

    def init_masks(self):
        for _ in range(0, 5):
            self.masks.append(
                Mask(self, self.ctx, self.app)
            )

    def on_position_update(self):
        self.init_polygon()
        self.manager.set_masks()
        self.app.app_settings.save()

    def move_left(self, jump=None):
        self.position = (
            self.position[0] + (jump or self.manager.shift_precision), self.position[1])
        self.on_position_update()

    def move_right(self, jump=None):
        self.position = (
            self.position[0] - (jump or self.manager.shift_precision), self.position[1])
        self.on_position_update()

    def move_up(self, jump=None):
        self.position = (
            self.position[0], self.position[1] + (jump or self.manager.shift_precision))
        self.on_position_update()

    def move_down(self, jump=None):
        self.position = (
            self.position[0], self.position[1] - (jump or self.manager.shift_precision))
        self.on_position_update()

    def update_masks(self, masks):
        for idx in range(0, 5):
            try:
                self.update_mask(masks[idx], idx)
            except IndexError:
                self.update_mask((0, 0, 0, 0), idx)

    def calc_mask_coords(self, mask, related=None):
        pos = related or self.position
        min_x, min_y, max_x, max_y = mask
        mask_coords = (
            min_x - pos[0],
            min_y - pos[1],
            max_x - pos[0],
            max_y - pos[1]
        )
        return mask_coords

    def update_mask(self, mask, idx):
        mask_obj = self.masks[idx]
        mask_obj.set_coords(self.calc_mask_coords(mask))

    def update_unmasked(self, unmasked):
        self.unmasked = unmasked

    def get_darken(self):
        if self.manager.active_monitor_idx == -1:
            return False
        if self.idx != self.manager.active_monitor_idx:
            return True
        return False

    def get_points(self, physical=False):
        if physical:
            pos = self.physical_position
        else:
            pos = self.position
        size = self.size

        return [
            (pos[0], pos[1]), (size[0] + pos[0], pos[1]),
            (size[0] + pos[0], size[1] + pos[1]), (pos[0], size[1] + pos[1])
        ]

    @property
    def viewport_args(self):
        pos = self.physical_position
        size = self.size
        total = self.manager.total
        return (pos[0], total[1]-pos[1]-size[1], size[0], size[1])

    @property
    def viewport_points(self):
        vp = self.viewport_args
        p_a = vp[0], vp[1]
        p_b = vp[0] + vp[2], vp[1]
        p_c = vp[0] + vp[2], vp[1] + vp[3]
        p_d = vp[0], vp[1] + vp[3]

        return (p_a, p_b, p_c, p_d)

    def set_viewport(self):
        self.ctx.viewport = self.viewport_args

    def get_projection_matrix(self):
        pos = self.position
        size = self.size
        # L   R    B    T
        return Matrix44.orthogonal_projection(pos[0], size[0]+pos[0], size[1]+pos[1], pos[1], -100, 1000)

    def init_polygon(self):
        self.polygon = geometry.Polygon(self.get_points())

    def render(self):
        self.set_viewport()
        self.app.mvp.write(self.get_projection_matrix().astype('f4').tobytes())
        self.app.darken.value = self.get_darken()
        self.app.render_main()
        for mask in self.masks:
            mask.render()


class OutputManager:
    def __init__(self, config, app, ctx):
        self.config = config
        self.monitors = []
        self.app = app
        self.ctx = ctx
        self.max_idx = 0

        self.init_monitors()
        self.unmask_image = Image.new(mode='RGBA', size=self.total)

        self.set_masks()
        self.active_monitor_idx = -1

        self.shift_prec_idx = 0
        self.prec_choices = [10, 5, 2, 1]

    def get_active_camera(self):
        if self.active_monitor_idx == -1:
            return None
        try:
            return self.monitors[self.active_monitor_idx]
        except IndexError:
            pass
        return None

    def draw_unmask_bitmap(self):
        self.app.render_lock = True
        self.unmask_image = Image.new(mode='RGBA', size=self.total)
        draw = ImageDraw.Draw(self.unmask_image)
        for mon in self.monitors:
            if not mon.unmasked:
                continue
            draw.polygon(list(mon.unmasked.exterior.coords), fill='#ff0000')
        if hasattr(self.app, 'unmasked_texture'):
            self.unmask_data = self.unmask_image.tobytes()
        self.app.render_lock = False

    @property
    def shift_precision(self):
        return self.prec_choices[self.shift_prec_idx]

    def toggle_precission(self):
        self.shift_prec_idx += 1
        if self.shift_prec_idx > len(self.prec_choices) - 1:
            self.shift_prec_idx = 0
        print("Precision:", self.shift_precision)

    def toggle_camera(self):
        self.active_monitor_idx += 1
        if self.active_monitor_idx >= self.max_idx:
            self.active_monitor_idx = -1

    @property
    def total(self):
        points = []
        for mon in self.monitors:
            for point in mon.get_points(physical=True):
                points.append(point)
        x_coordinates, y_coordinates = zip(*points)

        return (max(x_coordinates), max(y_coordinates))

    def init_monitors(self):
        i = 0
        for out in self.config:
            output = Output(out[0], out[1], out[2], self, self.app)
            output.idx = i
            self.monitors.append(output)
            i += 1
        self.max_idx = i

    def set_masks(self):
        masks = {}
        for mon1 in self.monitors:
            for mon2 in self.monitors:
                if mon1 == mon2:
                    continue
                if mon1.polygon.intersects(mon2.polygon):
                    intrs = mon1.polygon.intersection(mon2.polygon)
                    if not mon2 in masks:
                        masks[mon2] = []
                    masks[mon2].append(intrs.bounds)

        for mon in masks:
            unmasked = geometry.Polygon(mon.viewport_points)
            phy = mon.physical_position
            for mask in masks[mon]:
                mp = list(mon.calc_mask_coords(mask))
                mp[0] += phy[0]
                mp[1] += phy[1]
                mp[2] += phy[0]
                mp[3] += phy[1]
                pts = bounds_to_points(mp)

                mask_geom = geometry.Polygon(pts)
                unmasked -= mask_geom

            mon.update_masks(masks[mon])
            mon.update_unmasked(unmasked)

        self.draw_unmask_bitmap()
