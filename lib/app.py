import os
import threading

import pyvirtualdisplay
from easyprocess import EasyProcess


# "glmark2", "--fullscreen", "--run-forever", "-b", "jellyfish"

class XvfbApp(threading.Thread):
    def __init__(self, screen_size, setting):
        super().__init__()
        self.screen_size = screen_size

    def run(self):
        with pyvirtualdisplay.Display(backend='xvfb', size=self.screen_size, extra_args=['-fbdir', '/tmp']):
            os.environ['VGL_DISPLAY'] = ':1'
            with EasyProcess(["vglrun", "--", "/home/van/projects/knappenrode/demontage/start-vgl"]) as proc:
                proc.wait()
