import mmap
import threading
from datetime import datetime
 
import numpy

BENCH = 100

class XVFBCaptureAsync(threading.Thread):
    def __init__(self, reader, ctx):
        super().__init__()
        self.shmreader = reader
        self.ctx = ctx
        self.started = False
        self.times = []
        self.cnt = 0

    def start(self):
        if self.started:
            print('[!] Asynchroneous video capturing has already been started.')
            return None
        self.started = True
        self.thread = threading.Thread(target=self.update, args=())
        self.thread.start()
        return self

    def update(self):
        print("UPDATE")
        while self.started:
            d_start = datetime.now()
            print("WRITE...")
            self.ctx.grid_texture.write(self.shmreader.read())
            print("OK")
            d_end = datetime.now()
            print((d_end - d_start).total_seconds())
            
    def stop(self):
        self.started = False
        self.thread.join()
