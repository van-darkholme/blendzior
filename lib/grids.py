import ctypes
import math

import cairo


class Grid:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.surface_data = (ctypes.c_ubyte * (self.width * self.height * 4))()
        self.surface = cairo.ImageSurface.create_for_data(
            self.surface_data, cairo.FORMAT_ARGB32, self.width, self.height, self.width * 4)
        self.ctx = cairo.Context(self.surface)

    def draw(self):
        cr = self.ctx
        cr.select_font_face(
            "DejaVuSansMono", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
        cr.set_font_size(22)

        # Grid
        width, height = self.width, self.height
        x_spacing = 50
        y_spacing = 50

        cr.set_source_rgb(1, 1, 1)
        cr.set_line_width(1)

        cr.move_to(0, 0)
        cr.line_to(width, height)
        cr.stroke()

        cr.move_to(0, height)
        cr.line_to(width, 0)
        cr.stroke()

        for i in range(0, int(width / x_spacing) + 2):
            cr.set_source_rgb(1, 1, 1)
            cr.move_to(x_spacing * i, 0)
            cr.line_to(x_spacing * i, height)
            cr.stroke()

        for i in range(0, int(height / y_spacing) + 2):
            cr.set_source_rgb(1, 1, 1)
            cr.move_to(0, y_spacing * i)
            cr.line_to(width, y_spacing * i)
            cr.stroke()

        LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        ln = len(LETTERS)

        r = 0.0
        g = 0.0
        b = 0.0
        cstep = int(width / x_spacing) / 8
        for x in range(0, int(width / x_spacing) + 2):
            for y in range(0, int(height / y_spacing) + 2):
                r = (x % cstep) / cstep
                b = 1 - (y % cstep) / cstep
                g = (1 + math.sin(x)) / 2
                cr.set_source_rgb(r, g, b)
                cr.move_to(x_spacing * x + 11, y_spacing * y+31)
                cr.show_text(LETTERS[x % ln] + LETTERS[y % ln])

        return self.surface_data
