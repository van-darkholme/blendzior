from ctypes import *

try:
    RT = CDLL('librt.so')
except:
    RT = CDLL('librt.so.1')


libc = CDLL("libc.so.6")
get_errno_loc = libc.__errno_location
get_errno_loc.restype = POINTER(c_int)
IPC_CREAT = 512


def errcheck(ret, func, args):
    if ret == -1:
        e = get_errno_loc()[0]
        raise OSError(e)
    return ret

shmat = RT.shmat
shmat.argtypes = [c_int, POINTER(c_void_p), c_int]
shmat.restype = c_void_p
shmat.errcheck = errcheck

#print copen("nosuchfile", 0)

class SHMRead:
    def __init__(self, shmid, screen_size, header_size=3232):
        self.size = screen_size[0] * screen_size[1] * 4
        self.key = shmid
        self.addr = shmat(shmid, None, 0)
       
    def read(self):
        return string_at(self.addr, self.size)

    def dump(self):
        print(self.addr)
        OUTFILE = 'xd.xwd'
        f=open(OUTFILE, 'wb')
        f.write(string_at(self.addr, self.size))
        f.close()

if __name__ ==  "__main__":
    xd = SHMRead(98352, [3840,2160], 0)
    xd.dump()
