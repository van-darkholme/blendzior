import matplotlib.pyplot as plt

from math import pow

def bezier(x, y):
    xu = 0.0
    yu = 0.0
    i = 0
    for i in range(0, 101):
        u = i/100
        r = pow(1-u, 3)*x[0]+3*u*pow(1-u, 2)*x[1]+3 * pow(u, 2)*(1-u)*x[2] + pow(u, 3)*x[3]
        l = pow(1-u, 3)*y[0]+3*u*pow(1-u, 2)*y[1]+3 * pow(u, 2)*(1-u)*y[2] + pow(u, 3)*y[3]
        yield(r, l)

y = [0, 0.33, 0.66, 1]

x = [0.1, 0.75, 0.25, 1]

pts = list(bezier(x, y))
plt.plot(pts)
plt.show()
