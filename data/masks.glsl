#version 330

#if defined VERTEX_SHADER

uniform mat4 Mvp;

in vec3 in_position;
in vec3 in_normal;
in vec2 in_texcoord_0;

out vec3 v_vert;
out vec3 v_norm;
out vec2 v_text;

void main() {
  gl_Position = Mvp * vec4(in_position, 1.0);
  v_vert = in_position;
  v_norm = in_normal;
  v_text = in_texcoord_0;
}

#elif defined FRAGMENT_SHADER

float beiz(float u, float c1, float c2, float c3, float c4) {
  return pow(1 - u, 3) * c1 + 3 * u * pow(1 - u, 2) * c2 + 3 * pow(u, 2) * (1 - u) * c3 + pow(u, 3) * c4;
}

uniform int direction;
uniform bool debug;

uniform float c1;
uniform float c2;
uniform float c3;
uniform float c4;

vec3 v_color = vec3(0.0, 0.0, 0.0);
out vec4 f_color;
in vec2 v_text;

void main() {
  if (!debug) {
    if (direction == 1) {
      f_color = vec4(v_color, beiz(1 - v_text.y, c1, c2, c3, c4));
    }
    if (direction == 2) {
      f_color = vec4(v_color, beiz(v_text.y, c1, c2, c3, c4));
    }
    if (direction == 3) {
      f_color = vec4(v_color,  beiz(1 -v_text.x, c1, c2, c3, c4));
    }
    if (direction == 4) {
      f_color = vec4(v_color, beiz(v_text.x, c1, c2, c3, c4));
    }
  } else {
    f_color = vec4(vec3(1.0, .5, 1.0), .7);
  }

}

#endif
