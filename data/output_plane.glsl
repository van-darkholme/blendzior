#version 330

#if defined VERTEX_SHADER
uniform mat4 output_mvp;

in vec3 in_position;
in vec3 in_normal;
in vec2 in_texcoord_0;

out vec3 v_vert;
out vec3 v_norm;
out vec2 v_text;

void main() {
  gl_Position = output_mvp * vec4(in_position, 1.0);
  v_vert = in_position;
  v_norm = in_normal;
  v_text = in_texcoord_0;
}
#elif defined FRAGMENT_SHADER

uniform sampler2D Texture;
uniform sampler2D Unmask;

in vec3 v_vert;
in vec3 v_norm;
in vec2 v_text;
out vec4 f_color;

uniform float c1;
uniform float c2;
uniform float c3;
uniform float c4;


float beiz(float u, float c1, float c2, float c3, float c4) {
  return pow(1 - u, 3) * c1 + 3 * u * pow(1 - u, 2) * c2 + 3 * pow(u, 2) * (1 - u) * c3 + pow(u, 3) * c4;
}

void main() { 
    f_color = vec4(texture(Texture, v_text).rgb, 1.0);
    
    vec4 f_color_unmask = vec4(texture(Unmask, v_text).rgb, 1.0);
    
    if(f_color_unmask.r == 1) {
      f_color.r = beiz(f_color.r, c1, c2, c3, c4);
      f_color.g = beiz(f_color.g, c1, c2, c3, c4);
      f_color.b = beiz(f_color.b, c1, c2, c3, c4);
    }
}

#endif
