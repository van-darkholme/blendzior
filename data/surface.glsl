#version 330

#if defined VERTEX_SHADER
uniform mat4 Mvp;

in vec3 in_position;
in vec3 in_normal;
in vec2 in_texcoord_0;

out vec3 v_vert;
out vec3 v_norm;
out vec2 v_text;

void main() {
  gl_Position = Mvp * vec4(in_position, 1.0);
  v_vert = in_position;
  v_norm = in_normal;
  v_text = in_texcoord_0;
}

#elif defined FRAGMENT_SHADER

uniform bool darken;
uniform bool negative;
uniform sampler2D Texture;

in vec3 v_vert;
in vec3 v_norm;
in vec2 v_text;
out vec4 f_color;

const float ratio = 0.5;

void main() {
  vec4 t = texture(Texture, vec2(v_text.x, v_text.y));
  float alpha = 1.0;
  if (darken) {
    alpha = alpha * .5;
  }
  if (!negative) {
    f_color = vec4(t.r, t.g, t.b, alpha);
  } else {
    f_color = vec4(1 - t.r, 1 - t.g, 1 - t.b, alpha);
  }
}

#endif
