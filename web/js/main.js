function getHost () {
    var loc = window.location, new_uri;
    return loc.host.split(':')[0]
    //) + ":9883";
}

$(function () {
    var mqtt;
    var reconnectTimeout = 2000;
    var host = getHost();
    var port = 9883;

    function onConnect() {
        console.log("Connected ");
    }

    $('#toggle-cam').click((ev)=>{
        message = new Paho.Message("xd");
        message.destinationName = "blendzior/toggle_cam";
        mqtt.send(message);
    })

    $('#up, #down, #left, #right').click((ev)=>{
        message = new Paho.Message(ev.target.id + "," + $('#jump').val());
        message.destinationName = "blendzior/move";
        mqtt.send(message);
    })

    function onFailure(message) {
        console.log("Connection Attempt to Host " + host + "Failed");
        setTimeout(MQTTconnect, reconnectTimeout);
    }

    function uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
    function MQTTConnect() {
        console.log("connecting to " + host + " " + port);
        mqtt = new Paho.Client(host, port, "blendziorjs" + uuidv4());
        var options = {
            timeout: 3,
            onSuccess: onConnect,
            onFailure: onFailure,
            reconnect: true,
        };
        mqtt.connect(options);
    }

    var settings;
    function updateCurveGraphs() {
        
        var fnStr = '((1 - x) ^ 3) * c1 + 3 * x * ((1 - x) ^ 2) * c2 + 3 * (x ^ 2) * (1 - x) * c3 + (x ^ 3) * c4'
        var c = 0
        settings.edgeCurve.forEach((val) => {
            c += 1
            fnStr = fnStr.replace('c'+c, parseFloat($('#c' +c).val()))
        })
        var options = {
            grid: true,
            yAxis: {domain: [0, 1]},
            xAxis: {domain: [0, 1]},
            target: '#mask-curve',
            data: [{
              fn: fnStr,
              graphType: 'scatter',
              range: [0, 1]
            }]
        };
        functionPlot(options)
    }

    function updateFillGraphs() {
        
        var fnStr = '((1 - x) ^ 3) * c1 + 3 * x * ((1 - x) ^ 2) * c2 + 3 * (x ^ 2) * (1 - x) * c3 + (x ^ 3) * c4'
        var c = 0
        settings.eqLevelsCurve.forEach((val) => {
            c += 1
            fnStr = fnStr.replace('c'+c, parseFloat($('#f' +c).val()))
        })
        var options = {
            grid: true,
            yAxis: {domain: [0, 1]},
            xAxis: {domain: [0, 1]},
            target: '#fill-curve',
            data: [{
              fn: fnStr,
              graphType: 'scatter',
              range: [0, 1]
            }]
        };
        functionPlot(options)
    }

    fetch('/config.json')
        .then(response => response.json())
        .then(data => {
            settings = data;
            var c = 0
            settings.edgeCurve.forEach((val) => {
                c += 1
                $('#c' + c).val(val)
                $('#c' + c).change(() => {
                    message = new Paho.Message($('#c1').val() + ':' + $('#c2').val() + ':' + $('#c3').val() + ':' + $('#c4').val());
                    message.destinationName = "blendzior/mask_curve";
                    mqtt.send(message);
                    updateCurveGraphs()
                })
            })
            c = 0
            settings.eqLevelsCurve.forEach((val) => {
                c += 1
                $('#f' + c).val(val)
                $('#f' + c).change(() => {
                    message = new Paho.Message($('#f1').val() + ':' + $('#f2').val() + ':' + $('#f3').val() + ':' + $('#f4').val());
                    message.destinationName = "blendzior/fill_curve";
                    mqtt.send(message);
                    updateFillGraphs()
                })
            })
            updateFillGraphs()
            updateCurveGraphs()
        });    

    MQTTConnect();
});